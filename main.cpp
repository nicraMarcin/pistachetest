#include <iostream>
#include "pistache/endpoint.h"

using namespace Pistache;

class HelloHandler : public Http::Handler {
public:

HTTP_PROTOTYPE(HelloHandler)

    void onRequest(const Http::Request& request, Http::ResponseWriter response)  {
        response.send(Http::Code::Ok, "Hello, From Pistache");
    }
};

int main() {
    std::cout << "Starting Pistache" << std::endl;

    Address addr(Ipv4::any(), Port(8090));
    auto server = std::make_shared<Http::Endpoint>(addr);
    auto opts = Http::Endpoint::options().threads(2).flags(Tcp::Options::InstallSignalHandler);

    server->init(opts);
    server->setHandler(Http::make_handler<HelloHandler>());
    server->serveThreaded();
    while (true){
        sleep(1);
    };

    return 0;
}